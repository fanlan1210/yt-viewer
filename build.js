const path = require('path');
const builder = require('electron-builder');

builder.build({

	projectDir: path.resolve(__dirname),
	targets: builder.Platform.WINDOWS.createTarget(),
	config: {
		appId: 'net.fanlan.ytviewer.electorn.app',
		productName: 'YTViewer',
		directories: {
			output: './dist/win'
		},
		win: {
			icon: './icon.png',
			target: [
				{
					target: 'portable',
					arch: [
						'ia32',
//						'x64'
					]
				}
			]
		}
	}
});
