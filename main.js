const { app, BrowserWindow, Menu, dialog } = require('electron');
const fs = require('fs');
const path = require('path');
const ytdl = require('ytdl-core');

const menuTemplate = [
  {
	label: 'Download',
	submenu: [
	  {
		label: 'Direct',
	        accelerator: 'CmdOrCtrl+D',
	        click: function() {
		    // get current window's web url
		    const currentURL = mainWindow.webContents.getURL();
		    // get current window's web title
		    const currentTitle = mainWindow.webContents.getTitle();
		    // select file saving loaction
		    const paths = dialog.showSaveDialogSync(mainWindow, {defaultPath: currentTitle + '.mp4'});
		    // download url by ytdl-core and open file in explorer
		    ytdl(currentURL)
		      .pipe(fs.createWriteStream(paths).on('close',()=>{
			  dialog.showMessageBox(mainWindow, {message: 'Download complete!'});
		      }));
		}
	  }
	]
  }
]

const menu = Menu.buildFromTemplate(menuTemplate);
Menu.setApplicationMenu(menu);

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
	width: 1280,
	height: 720,
	/*webPreferences: {
	  nodeIntegration: true
	}*/
  });

  // browse YouTube
  mainWindow.loadURL('https://www.youtube.com/');
  
  //mainWindow.loadFile('index.html');

  mainWindow.on('closed', function() {
	mainWindow = null;
  });
}

app.on('ready', createWindow);

// exit when all windows are closed
app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
	app.quit();
  }
});

app.on('activate', function() {
  if (mainWindow === null) {
	createWindow();
  }
});
